import "./box.css";

const smallBox = <div>small lightblue box</div>;
const mediumBox = <div>medium pink box</div>;
const largeBox = <div>large orange box</div>;

const BoxComponent = () => {
  return (
    <div>
      {smallBox}
      {mediumBox}
      {largeBox}
    </div>
  );
};

export default BoxComponent;
