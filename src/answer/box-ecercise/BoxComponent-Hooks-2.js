import React from "react";
import "./box.css";

const BoxColourFulContext = React.createContext({
    boxTypeItems: [],
    addBoxToArray: (boxType) => { },
    removeBoxFromArray: (indexToDelete) => { },
});

class Box extends React.Component {
    render() {
        let styleBox = "box";

        switch (this.props.type) {
            case "small":
                styleBox = styleBox + " box--small";
                break;
            case "medium":
                styleBox = styleBox + " box--medium";
                break;
            case "large":
                styleBox = styleBox + " box--large";
                break;
            default:
        }

        return (
            <BoxColourFulContext.Consumer>
                {(context) => {
                    return (
                        <div className={styleBox} style={{ fontStyle: "italic" }}>
                            <div style={{ flex: 1, justifyContent: "flex-end" }}>{`${this.props.indexArray + 1
                                }. ${this.props.type} box`}</div>
                            <div>
                                <button
                                    style={{
                                        backgroundColor: "red",
                                        color: "white",
                                        width: "100%",
                                    }}
                                    onClick={() => {
                                        context.removeBoxFromArray(this.props.indexArray);
                                    }}
                                >
                                    delete
                                </button>
                            </div>
                        </div>
                    );
                }}
            </BoxColourFulContext.Consumer>
        );
    }
}

const ColourFulBoxComponent = () => {
    const [selectedBoxType, setSelectedBoxType] = React.useState("small");
    const {addBoxToArray, boxTypeItems} = React.useContext(BoxColourFulContext);

    const handleSelectChanges = React.useCallback(
        (event) => setSelectedBoxType(event.target.value),
        []
    );
    const handleButtonAddClick = React.useCallback(() => {
        addBoxToArray(selectedBoxType);
    }, [selectedBoxType, addBoxToArray]);

    return (
        <div style={{ display: "flex" }}>
            <div style={{ flex: 1 }}>
                {boxTypeItems.map((boxType, index) => (
                    <Box key={index.toString()} type={boxType} indexArray={index} />
                ))}
            </div>
            <div style={{ flex: 1 }}>
                <select name="box" id="boxstype" onChange={handleSelectChanges}>
                    <option value="small">small</option>
                    <option value="medium">medium</option>
                    <option value="large">large</option>
                </select>
                <button onClick={handleButtonAddClick}>+</button>
            </div>
        </div>
    );
};

const ColourFulBoxContainerComponent = () => {
    const [boxTypeItems, setBoxTypeItems] = React.useState([]);

    const addBoxToArray = (boxType) => {
        setBoxTypeItems(...boxTypeItems, boxType);
    };

    const removeBoxFromArray = (indexToDelete) => {
        const newArray = this.state.boxTypeItems.filter(
            (_, index) => index !== indexToDelete
        );
        setBoxTypeItems(newArray);
    };

    return (
        <BoxColourFulContext.Provider
            value={{
                boxTypeItems: boxTypeItems,
                addBoxToArray: addBoxToArray,
                removeBoxFromArray: removeBoxFromArray,
            }}
        >
            <ColourFulBoxComponent />
        </BoxColourFulContext.Provider>
    );
};

export default ColourFulBoxContainerComponent;
