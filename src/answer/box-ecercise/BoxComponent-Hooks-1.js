import React from "react";
import "./box.css";

class Box extends React.Component {
  render() {
    let boxStyle = "box";
    let propsStyle = { fontStyle: "italic" };

    switch (this.props.type) {
      case "small":
        boxStyle = boxStyle.concat(" box--small");
        propsStyle = { ...propsStyle, backgroundColor: "lightblue" };
        break;
      case "medium":
        boxStyle = boxStyle.concat(" box--medium");
        propsStyle = { ...propsStyle, backgroundColor: "pink" };
        break;
      case "large":
        boxStyle = boxStyle.concat(" box--large");
        propsStyle = { ...propsStyle, backgroundColor: "orange" };
        break;
      default:
    }

    return (
      <div className={boxStyle} style={propsStyle}>
        {this.props.type} box
      </div>
    );
  }
}

const BoxContainerComponent = () => {
  const [selectedBoxType, setSelectedBoxType] = React.useState("small");
  const [boxTypes, setBoxTypes] = React.useState(["large"]);
  const [isBoxFull, setIsBoxFull] = React.useState(false);

  const handleSelectChanges = React.useCallback(
    (event) => setSelectedBoxType(event.target.value),
    []
  );

  const handleButtonAddClick = React.useCallback(() => {
    setBoxTypes([...boxTypes, selectedBoxType]);
  }, [selectedBoxType, boxTypes]);

  React.useEffect(() => {
    if (boxTypes.length < 3) {
      setIsBoxFull(false);
    } else {
      setIsBoxFull(true);
    }
  }, [boxTypes]);

  return (
    <div style={{ display: "flex", flexDirection: "row" }}>
      <div style={{ flex: 1 }}>
        {boxTypes.map((boxType, index) => (
          <Box key={index.toString()} type={boxType} />
        ))}
      </div>
      <div style={{ flex: 1 }}>
        <div style={{ marginBottom: "20px" }}>
          please choose box type to add the box:
        </div>
        <select name="boxs" onChange={handleSelectChanges}>
          <option value="small">small</option>
          <option value="medium">medium</option>
          <option value="large">large</option>
        </select>
        {!isBoxFull && <button onClick={handleButtonAddClick}>+</button>}
      </div>
    </div>
  );
};

export default BoxContainerComponent;
