import "./box.css";

const smallBox = (
  <div
    className="box box--small"
    style={{ fontStyle: "italic", backgroundColor: "lightblue" }}
  >
    small box
  </div>
);

const mediumBox = (
  <div
    className="box box--medium"
    style={{ fontStyle: "italic", backgroundColor: "pink" }}
  >
    medium box
  </div>
);

const largeBox = (
  <div
    className="box box--large"
    style={{ fontStyle: "italic", backgroundColor: "orange" }}
  >
    large box
  </div>
);

const BoxComponent = () => {
  return (
    <div>
      {smallBox}
      {mediumBox}
      {largeBox}
    </div>
  );
};

export default BoxComponent;
