import React from "react";
import "./box.css";

class Box extends React.Component {
  render() {
    let boxStyle = "box";
    let propsStyle = { fontStyle: "italic" };

    switch (this.props.type) {
      case "small":
        boxStyle = boxStyle.concat(" box--small");
        propsStyle = { ...propsStyle, backgroundColor: "lightblue" };
        break;
      case "medium":
        boxStyle = boxStyle.concat(" box--medium");
        propsStyle = { ...propsStyle, backgroundColor: "pink" };
        break;
      case "large":
        boxStyle = boxStyle.concat(" box--large");
        propsStyle = { ...propsStyle, backgroundColor: "orange" };
        break;
      default:
    }

    return (
      <div className={boxStyle} style={propsStyle}>
        {this.props.type} box
      </div>
    );
  }
}

const BoxComponent = () => {
  return (
    <div>
      <Box type="small" />
      <Box type="medium" />
      <Box type="large" />
    </div>
  );
};

export default BoxComponent;
