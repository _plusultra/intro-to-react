import React from "react";
import "./box.css";

class Box extends React.Component {
  render() {
    let boxStyle = "box";
    let propsStyle = { fontStyle: "italic" };

    switch (this.props.type) {
      case "small":
        boxStyle = boxStyle.concat(" box--small");
        propsStyle = { ...propsStyle, backgroundColor: "lightblue" };
        break;
      case "medium":
        boxStyle = boxStyle.concat(" box--medium");
        propsStyle = { ...propsStyle, backgroundColor: "pink" };
        break;
      case "large":
        boxStyle = boxStyle.concat(" box--large");
        propsStyle = { ...propsStyle, backgroundColor: "orange" };
        break;
      default:
    }

    return (
      <div className={boxStyle} style={propsStyle}>
        {this.props.type} box
       
      </div>
    );
  }
}

class BoxContainer extends React.Component {
  state = { boxTypes: ["small"], selectedBoxType: "small" };
  render = () => {
    return (
      <div style={{ display: "flex", flexDirection: "row" }}>
        <div style={{ flex: 1 }}>
          {this.state.boxTypes.map((boxType, index) => (
            <Box key={index.toString()} type={boxType} />
          ))}
        </div>
        <div style={{ flex: 1 }}>
          <div style={{ marginBottom: "20px" }}>
            please choose box type to add the box:
          </div>
          <select
            name="boxs"
            onChange={(event) =>
              this.setState({
                ...this.state,
                selectedBoxType: event.target.value,
              })
            }
          >
            <option value="small">small</option>
            <option value="medium">medium</option>
            <option value="large">large</option>
          </select>
          <button
            onClick={() => {
              this.setState({
                ...this.state,
                boxTypes: [...this.state.boxTypes, this.state.selectedBoxType],
              });
            }}
          >
            +
          </button>
        </div>
      </div>
    );
  };
}

export default BoxContainer;
